from twine.plot.diagram import Network
from twine.plot.diagram import Attribute
from twine.plot.diagram import Architecture

nw = Network('test')

a1 = Attribute('web01')
a1.key_values['address'] = '127.1.x.x/24'

a2 = Attribute('web02')
a2.key_values['address'] = '127.2.x.x/24'

nw.attributes.append(a1)
nw.attributes.append(a2)

arch = Architecture()
arch.networks.append(nw)

print(arch.draw())
