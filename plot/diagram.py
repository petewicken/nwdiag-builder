import abc


class Diagram(object):
    __metaclass__ = abc.ABCMeta

    def get_part_diagrams(self, parts):
        part_strings = []
        for part in parts:
            part_string = ''.join(part.diagram())
            part_strings.append(part_string)

        return part_strings

    @abc.abstractmethod
    def diagram():
        return NotImplemented

    def draw(self):
        return ''.join(self.diagram())


class Architecture(Diagram):

    def __init__(self):
        self.networks = []

    def diagram(self):
        if not self.networks:
            raise Exception('There must be at least one network!')

        diagram_parts = []

        open_bracket = '{'
        close_bracket = '}'

        diagram_parts.append(open_bracket)

        networks = self.get_part_diagrams(self.networks)
        for network in networks:
            diagram_parts.append(network)

        diagram_parts.append(close_bracket)

        return diagram_parts


class Attribute(Diagram):

    def __init__(self, name):
        self.name = name
        self.key_values = {}

    def diagram(self):
        diagram_parts = []

        semicolon = ';'
        open_sqbracket = '['
        close_sqbracket = ']'
        comma = ','

        diagram_parts.append(self.name)
        diagram_parts.append(open_sqbracket)

        for key, value in self.key_values.iteritems():
            diagram_parts.append('{0} = "{1}"'.format(key, value))
            diagram_parts.append(comma)
        diagram_parts.pop()

        diagram_parts.append(close_sqbracket)
        diagram_parts.append(semicolon)

        return diagram_parts


class Network(Diagram):

    def __init__(self, name, address=None):
        self.name = name
        self.address = address
        self.attributes = []

    def diagram(self):
        diagram_parts = []

        open_bracket = '{'
        close_bracket = '}'
        semicolon = ';'

        node_name = self.__class__.__name__.lower()
        declaration = '{0} {1} '.format(node_name, self.name)
        diagram_parts.append(declaration)
        diagram_parts.append(open_bracket)

        if self.address:
            address = 'address = "{0}"'.format(self.address)
            diagram_parts.append(address)
            diagram_parts.append(semicolon)

        if self.attributes:
            attributes = self.get_part_diagrams(self.attributes)

            for attribute in attributes:
                diagram_parts.append(attribute)

        diagram_parts.append(close_bracket)

        return diagram_parts
